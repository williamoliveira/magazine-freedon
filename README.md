Front end test for magalu

## technologies used

- javascript
- eslint
- jest
- webpack
- typescript
- html
- scss/css

Tools used

## Normalize

[Normalize](https://ageek.dev/normalize-css)

## Icons

[flaticon](https://www.flaticon.com/free-icon/)

## Diagram

![Diagram](/diagram/magazine-freedon.png)
