export interface IProduct {
  id: number;
  sku: number;
  title: string;
  description: string;
  availableSizes: Partial<IAvailableSizes>;
  style: string;
  price: number;
  installments: number;
  currencyId: string;
  currencyFormat: string;
  isFreeShipping: boolean;
  image: string;
}

interface IAvailableSizes {
  S: number;
  G: number;
  GG: number;
  GGG: number;
}
