import { IProduct } from "../interfaces/iproducts";
import { Storage } from "./storage.service";

export class CardService {
  /**
   * Generate the card template
   *
   * @param image { string }
   * @param title { string }
   * @param price { number }
   * @param id { number }
   * @param whishlist { string }
   * @param closed { boolean }
   * @returns string
   */
  private getTemplate(image: string, title: string, price: number, id: number, whishlist: string, closed = false): string {
    return `
     <div class="card" item="${id}">
        ${this.getTemplateFlag(whishlist, closed)}
        ${this.getTemplateClosed(closed)}
        <div class="card__wrapper">
          <div class="card__image">
            <img src="${image}"  />
          </div>
          <div class="card__title"><b>${title}</b></div>
          <div class="card__price"><b>R$ ${(Math.round(price * 100) / 100).toFixed(2)}</b></div>
        </div>
      </div>
    `;
  }
  /**
   * Generate the flag template
   *
   * @param whishlist
   * @param closed
   * @returns
   */
  getTemplateFlag(whishlist: string, closed: boolean): string {
    return !closed
      ? ` <div class="card__icon--heart">
                <img src="/assets/flag.png"  />
                <img class="heart" src="/assets/${whishlist}.png"  />
            </div>`
      : "";
  }
  /**
   * Generates the close button template
   *
   * @param exists boolean
   * @returns string
   */
  getTemplateClosed(exists: boolean): string {
    return exists ? `<div class="card__icon--closed">x</div>` : "";
  }
  /**
   * check if it's on the wish list
   *
   * @param exists boolean
   * @returns string
   */
  itOnWishList(exists: boolean): string {
    return exists ? "heart" : "heart-white";
  }
  /**
   * build templates for all cards
   *
   * @param products
   * @returns string
   */
  createCardList(products: IProduct[]): string {
    const storage: Storage = new Storage();
    const items: string[] = storage.getItemsWhishlist();

    return products
      .map((product: IProduct) => {
        const { image, title, price, id } = product;
        const wishlist: string = this.itOnWishList(items.includes(id.toString()));
        return this.getTemplate(image, title, price, id, wishlist);
      })
      .join("");
  }
  /**
   * builds the templates of the cards that are on the wish list
   *
   * @param products { IProduct[] }
   * @returns string
   */
  createCardListWhishlist(products: IProduct[]): string {
    const storage: Storage = new Storage();
    const items: string[] = storage.getItemsWhishlist();

    return products
      .map((product: IProduct) => {
        const { image, title, price, id } = product;
        if (items.includes(id.toString())) {
          const wishlist: string = this.itOnWishList(items.includes(id.toString()));
          return this.getTemplate(image, title, price, id, wishlist, true);
        }
      })
      .join("");
  }
}
