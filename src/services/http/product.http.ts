import { Observable } from "rxjs";
import { ajax } from "rxjs/ajax";
import { pluck } from "rxjs/operators";
import { IProduct } from "../../interfaces/iproducts";
import { API_URL } from "./config.http";
export class ProductHttp {
  public getProducts(): Observable<unknown | IProduct[]> {
    return (
      ajax
        .getJSON(API_URL)
        ///
        .pipe(pluck("products"))
    );
  }
}
