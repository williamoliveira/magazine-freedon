export abstract class RenderService {
  element: HTMLElement;
  public async render(seletor: string, template: string): Promise<void> {
    this.element = document.querySelector(seletor);
    await this.element.insertAdjacentHTML("beforeend", template);
    this.afterRender();
  }

  ///Implementation
  abstract afterRender(): void;
}
