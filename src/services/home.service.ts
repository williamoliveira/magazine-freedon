import { IProduct } from "../interfaces/iproducts";
import { CardRenderService } from "./card.render.service";
import { CardService } from "./card.service";
import { ProductHttp } from "./http/product.http";

export class HomeService {
  constructor() {
    /// instantiates the objects to be used
    const products = new ProductHttp();
    const card = new CardService();
    const cardRender = new CardRenderService();

    let cardListTemplate;
    /// retrieve the products
    products.getProducts().subscribe((data: IProduct[]) => {
      ///retrieve the page
      const isHome = document.querySelector("body").classList.contains("home");

      if (isHome) cardListTemplate = card.createCardList(data);
      else cardListTemplate = card.createCardListWhishlist(data);

      /// render cards
      cardRender.render("main", cardListTemplate);
    });
  }
}
