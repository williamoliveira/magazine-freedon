import { RenderService } from "./render.service";
import { Storage } from "./storage.service";

export class CardRenderService extends RenderService {
  afterRender(): void {
    Array.from(this.element.children as HTMLCollection).forEach((item) => {
      item.querySelector(".card__icon--heart") && item.querySelector(".card__icon--heart").addEventListener("click", this.addRemoveWishlist);
      item.querySelector(".card__icon--closed") && item.querySelector(".card__icon--closed").addEventListener("click", this.removeCard);
    });
  }
  /**
   * add or remove a wishlist item via event
   *
   * @param event { UIEvent }
   */
  addRemoveWishlist(event: UIEvent): void {
    const { target } = event;

    const card: HTMLElement = (target as HTMLElement).closest(".card");

    // when separating into more methods the application loses the reference
    // for this reason, the implementation below is part of this method.

    const storage = new Storage();

    const id = card.getAttribute("item");

    const exists = storage.toggleItem(id);

    const heart = card.querySelector(".heart").getAttribute("src");

    if (exists) card.querySelector(".heart").setAttribute("src", heart.replace("heart-white", "heart"));
    else card.querySelector(".heart").setAttribute("src", heart.replace("heart", "heart-white"));
  }
  /**
   * remove an item from the wish list via event
   *
   * @param event { UIEvent }
   */
  removeCard(event: UIEvent): void {
    const { target } = event;

    const card: HTMLElement = (target as HTMLElement).closest(".card");

    const id = card.getAttribute("item");

    const storage = new Storage();

    storage.removeItemWhishlist(id);

    card.remove();
  }
}
