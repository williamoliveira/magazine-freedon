export class Storage {
  private key = "whishlist";

  /**
   * save an item in the list
   *
   * @param id { string }
   */
  public addItemWhishlist(id: string): void {
    const items: string[] = this.getItemsWhishlist();
    // save if it doesn't exist
    if (!items.includes(id)) {
      items.push(id);
      this.saveItems(items);
    }
  }

  /**
   * add or remove an item
   *
   * @param id { string }
   */
  public toggleItem(id: string): boolean {
    const items: string[] = this.getItemsWhishlist();
    // save if it doesn't exist
    if (!items.includes(id)) {
      items.push(id);
      this.saveItems(items);
      return true;
    } else {
      this.removeItemWhishlist(id);
      return false;
    }
  }

  /**
   * retrieves all items from the list
   *
   * @returns { string[] }
   */
  public getItemsWhishlist(): string[] {
    return JSON.parse(localStorage.getItem(this.key)) || [];
  }

  /**
   * remove an item from the list
   *
   * @returns void
   */
  public removeItemWhishlist(id: string): void {
    const items: string[] = this.getItemsWhishlist().filter((item) => item != id);
    this.saveItems(items);
  }

  /**
   * save items
   *
   * @param items { string[] }
   */
  private saveItems(items: string[]): void {
    localStorage.setItem(this.key, JSON.stringify(items));
  }

  /**
   * clear list
   */
  public clearWhishlist(): void {
    localStorage.clear();
  }
}
