import { ProductHttp } from "../../src/services/http/product.http";
import { ajax } from "rxjs/ajax";
import { of } from "rxjs";
import { IProduct } from "../../src/interfaces/iproducts";

let service: ProductHttp;

describe("Testing services for ProductHttp", function () {
  beforeEach(() => {
    service = new ProductHttp();
  });

  it("service ProductHttp must exist", function () {
    expect(service).toBeTruthy();
  });

  it("the `getProducts` method must exist", function () {
    expect(service.getProducts).toBeInstanceOf(Function);
  });

  it("the `getProducts` method must be implemented ", function () {
    const product: IProduct[] = [
      {
        id: 0,
        sku: 8552515751438644,
        title: "Camisa Nike Corinthians I",
        description: "14/15 s/nº",
        availableSizes: {
          S: 100,
          G: 5,
          GG: 120,
          GGG: 12,
        },
        style: "Branco com listras pretas",
        price: 229.9,
        installments: 9,
        currencyId: "BRL",
        currencyFormat: "R$",
        isFreeShipping: true,
        image: "https://via.placeholder.com/300x300",
      },
    ];

    jest.spyOn(ajax, "getJSON").mockReturnValue(
      of({
        products: product,
      })
    );

    service.getProducts().subscribe((data) => {
      expect(data).toEqual(product);
    });
  });
});
