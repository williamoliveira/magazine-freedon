import { CardService } from "../src/services/card.service";

let service: CardService;
describe("Testing services for CardService", function () {
  beforeEach(() => {
    service = new CardService();
  });

  it("service CardService must exist", function () {
    expect(service).toBeTruthy();
  });
});
