import { Storage } from "../src/services/storage.service";

let service: Storage;

describe("Testing services for Storage", function () {
  beforeEach(() => {
    service = new Storage();
  });

  it("service Storage must exist", function () {
    expect(service).toBeTruthy();
  });
});
