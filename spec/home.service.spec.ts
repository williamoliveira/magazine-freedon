import { HomeService } from "../src/services/home.service";
let service: HomeService;
describe("Testing services for HomeService", function () {
  beforeEach(() => {
    service = new HomeService();
  });

  it("service HomeService must exist", function () {
    expect(service).toBeTruthy();
  });
});
