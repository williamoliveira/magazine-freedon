import { CardRenderService } from "../src/services/card.render.service";

let service: CardRenderService;
describe("Testing services for CardRenderService", function () {
  beforeEach(() => {
    service = new CardRenderService();
  });

  it("service CardRenderService must exist", function () {
    expect(service).toBeTruthy();
  });
});
